﻿using System;
namespace OOPRefreshLibrary
{
    public abstract class Person
    {
        public string Fname { get; set; }
        public string Lname { get; set; }
        public enum EGender { M, F, NS };
        private EGender Gender;

        public Person(string Fname, string Lname, EGender Gender)
        {
            this.Fname = Fname;
            this.Lname = Lname;
            this.Gender = Gender;
        }

        public Person(EGender Gender)
        {
            this.Gender = Gender;
            Lname = "Doe";

            if (Gender == EGender.M) 
            {
                Fname = "John";
            }
            else 
            {
                Fname = "Jane";
            }
        }

        public abstract string Work();

        public string GetGender()
        {
            if (this.Gender == EGender.M)
            {
                return "Male";
            }
            else if (this.Gender == EGender.F)
            {
                return "Female";
            }
            else return "Gender not specified";
        }

        public virtual string GetOccupation()
        {
            return "Unemployed";
        }
    }
}
