﻿using System;
namespace OOPRefreshLibrary
{
    public class SpecialNeedsTeacher : Teacher
    {
        public override string GetOccupation()
        {
            return "Special Needs Teacher";
        }

        public SpecialNeedsTeacher(int StaffID, string Fname, string Lname, EGender Gender) : base (StaffID, Fname, Lname, Gender)
        {
            this.TeachBehaviour = new TeachSpecialNeeds();
        }
    }
}
