﻿using System;
namespace OOPRefreshLibrary
{
    public class Hobo : Person
    {
        public Hobo(string Fname, string Lname, EGender Gender) : base (Fname, Lname, Gender) {}

        public Hobo(EGender Gender) : base (Gender) {}

        public override string Work()
        {
            return "Find a place to sleep";
        }
    }
}
