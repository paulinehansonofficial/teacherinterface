﻿using System;
namespace OOPRefreshLibrary
{
    public class SecondaryTeacher : Teacher
    {
        public override string GetOccupation()
        {
            return "Secondary Teacher";
        }

        public SecondaryTeacher(int StaffID, string Fname, string Lname, EGender Gender) : base (StaffID, Fname, Lname, Gender)
        {
            this.TeachBehaviour = new TeachHighSchool();
        }
    }
}
