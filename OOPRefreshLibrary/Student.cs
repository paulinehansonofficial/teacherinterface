﻿using System;
namespace OOPRefreshLibrary
{
    public class Student : Person
    {
        public int pStudentID { get; set; }

        public Student(int StudentID, string Fname, string Lname, EGender Gender) : base (Fname, Lname, Gender)
        {
            pStudentID = StudentID;
        }

        public override string Work()
        {
            return "Study, drink coffee, sleep, repeat";
        }
    }
}
