﻿using System;
namespace OOPRefreshLibrary
{
    public abstract class Teacher : Person
    {
        private int pStaffID { get; set; }

        protected ITeach TeachBehaviour;

        public override string GetOccupation()
        {
            return "Teacher";
        }

        public override string Work()
        {
            return TeachBehaviour.Teach();
        }

        public Teacher(int StaffID, string Fname, string Lname, EGender Gender) : base (Fname, Lname, Gender)
        {
            this.pStaffID = StaffID;
        }
    }
}
