﻿using System;
namespace OOPRefreshLibrary
{
    public class PAVETeacher : Teacher
    {
        public override string GetOccupation()
        {
            return "PAVE Teacher";
        }

        public PAVETeacher(int StaffID, string Fname, string Lname, EGender Gender) : base (StaffID, Fname, Lname, Gender)
        {
            this.TeachBehaviour = new TeachPAVE();
        }
    }
}
